/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
enum ActionTypes {
  SET_BOOKMARK = 'SET_BOOKMARK',
  REMOVE_BOOKMARK = 'REMOVE_BOOKMARK',
  REMOVE_ALL_BOOKMARKS = 'REMOVE_ALL_BOOKMARKS',
  SET_HIDE = 'SET_HIDE',
}

export default ActionTypes;
