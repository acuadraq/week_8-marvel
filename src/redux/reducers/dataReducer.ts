/* eslint-disable no-case-declarations */
/* eslint-disable implicit-arrow-linebreak */
import { Bookmark, HideAction, SearchBookAction } from '../actions/actions';
import ActionTypes from '../constants/action-types';

const addToLocal = (state: Bookmark[], action: SearchBookAction) => {
  if (action.type === ActionTypes.SET_BOOKMARK) {
    const check: Bookmark[] = state.filter(
      item => item.bookmark.id === action.payload.bookmark.bookmark.id,
    );
    if (check.length > 0) {
      return [...state];
    }

    return [...state, action.payload.bookmark];
  }
  return [];
};

const removeOne = (state: Bookmark[], action: SearchBookAction) => {
  if (action.type === ActionTypes.REMOVE_BOOKMARK) {
    const arr: Bookmark[] = [...state];
    arr.splice(action.payload.id, 1);
    return [...arr];
  }
  return [...state];
};

export const bookmarkReducer = (state: Bookmark[] = [], action: SearchBookAction): Bookmark[] => {
  switch (action.type) {
    case ActionTypes.SET_BOOKMARK:
      const localStg = addToLocal(state, action);
      return localStg;
    case ActionTypes.REMOVE_BOOKMARK:
      const bookUpdated = removeOne(state, action);
      return bookUpdated;
    case ActionTypes.REMOVE_ALL_BOOKMARKS:
      return [];
    default:
      return state;
  }
};

const addHide = (state: number[], action: HideAction) => {
  if (action.type === ActionTypes.SET_HIDE) {
    return [...state, action.payload.id];
  }
  return [];
};

export const hideReducer = (state: number[] = [], action: HideAction): number[] => {
  switch (action.type) {
    case ActionTypes.SET_HIDE:
      const adding = addHide(state, action);
      return adding;
    default:
      return state;
  }
};
