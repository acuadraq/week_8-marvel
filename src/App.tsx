import React from 'react';
import { useRoutes } from 'react-router-dom';
import CharacterDetail from './components/CharacterDetail/CharacterDetail';
import Characters from './components/Characters/Characters';
import Comics from './components/Comics/Comics';
import ComicDetail from './components/ComicDetail/ComicDetail';
import StoryDetail from './components/StoryDetail/StoryDetail';
import Home from './components/Home/Home';
import NotFound from './components/NotFound';
import Stories from './components/Storie/Stories';
import RoutesNames from './customroutes/routesNames';
import MainRoute from './customroutes/MainLayout';
import SecondRoute from './customroutes/SecondLayout';
import Bookmarks from './components/Bookmark/Bookmarks';

const App = () => {
  const mainRoutes = {
    path: RoutesNames.home,
    element: <MainRoute />,
    children: [
      { path: RoutesNames.home, element: <Home /> },
      { path: RoutesNames.characters, element: <Characters /> },
      { path: RoutesNames.charactesDetail, element: <CharacterDetail /> },
      { path: RoutesNames.comicsDetail, element: <ComicDetail /> },
      { path: RoutesNames.comics, element: <Comics /> },
      { path: RoutesNames.stories, element: <Stories /> },
      { path: RoutesNames.storiesDetail, element: <StoryDetail /> },
      { path: RoutesNames.bookmarks, element: <Bookmarks /> },
    ],
  };

  const secondRoutes = {
    path: '',
    element: <SecondRoute />,
    children: [{ path: '*', element: <NotFound /> }],
  };

  const routing = useRoutes([mainRoutes, secondRoutes]);
  return <>{routing}</>;
};
export default App;
