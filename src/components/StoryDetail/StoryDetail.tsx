/* eslint-disable react/jsx-no-target-blank */
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { ICharacter } from '../../interfaces/characters';
import { IComic } from '../../interfaces/comics';
import { IStories } from '../../interfaces/stories';
import { removeBookmark, setBookmark, setHide } from '../../redux/actions/actions';
import NotFound from '../NotFound';
import bookmark from '../bookmark.png';
import { getStory, getStoryCharacters, getStoryComics } from '../../adapters/Fetch';
import ComicCharacters from '../ComicDetail/ComicCharacters';
import CharacterComics from '../CharacterDetail/CharacterComics';
import { AppState } from '../../redux/reducers';
import hide from '../hide.png';

type Bookmark = {
  bookmark: {
    id: number | undefined;
    name: string | undefined;
    thumbnail: string | undefined;
    type: string | undefined;
  };
};

const StoryDetail = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [storyInfo, setStoryInfo] = useState<IStories>();
  const [characters, setCharacters] = useState<ICharacter>();
  const [comics, setComics] = useState<IComic>();
  const [error, setError] = useState<boolean>(false);
  const dispatch = useDispatch();
  const resourceHidden: number[] = useSelector((state: AppState) => state.hides);
  const bookmarks: Bookmark[] = useSelector((state: AppState) => state.bookmarks);
  const { id } = useParams<string>();

  const getComics = async () => {
    if (id) {
      const comic: IComic = await getStoryComics(parseInt(id, 10));
      await setComics(comic);
      setLoading(false);
    }
  };

  const getCharacters = async () => {
    if (id) {
      const character: ICharacter = await getStoryCharacters(parseInt(id, 10));
      await setCharacters(character);
      getComics();
    }
  };

  const getStoryInfo = async () => {
    if (id) {
      if (resourceHidden.includes(parseInt(id, 10))) {
        setError(true);
        setLoading(false);
        return;
      }
      const story: IStories = await getStory(parseInt(id, 10));
      if (story.code === 404) {
        setError(true);
        setLoading(false);
        return;
      }
      await setStoryInfo(story);
      getCharacters();
    }
  };

  const saveBookmark = () => {
    const story = {
      bookmark: {
        id: storyInfo?.data.results[0].id,
        name: storyInfo?.data.results[0].title,
        thumbnail: 'https://m.media-amazon.com/images/I/51B+S8JiAaL.jpg',
        type: 'stories',
      },
    };
    dispatch(setBookmark(story));
  };

  const hideElement = (num: number) => {
    const isInBookmark = bookmarks.map(e => e.bookmark.id).indexOf(num);
    dispatch(setHide(num));
    if (isInBookmark !== -1) dispatch(removeBookmark(isInBookmark));
    window.location.reload();
  };

  useEffect(() => {
    setLoading(true);
    getStoryInfo();
  }, []);

  return (
    <section>
      <div className="container">
        {loading && <div className="animation__loading" />}
        {!loading && (
          <>
            {error ? (
              <NotFound />
            ) : (
              storyInfo?.data.results.map(story => (
                <div key={story.id}>
                  <div className="character__banner">
                    <div className="image__container">
                      <img src="https://m.media-amazon.com/images/I/51B+S8JiAaL.jpg" alt="" />
                    </div>
                    <div className="character__content">
                      <h3>{story.title}</h3>
                      <div className="overview-button">
                        <span className="overview">Overview</span>
                        <button type="button" onClick={() => hideElement(story.id)}>
                          <span>Hide</span>
                          <img src={hide} alt="Hide" width="20" height="20" />
                        </button>
                      </div>
                      <p>
                        {story.description
                          ? story.description
                          : 'This story doesnt have a description'}
                      </p>
                      <div className="buttons">
                        <button type="button" onClick={saveBookmark}>
                          <span>Save</span>
                          <img src={bookmark} alt="Bookmark" width="20" height="20" />
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="items__additionals">
                    <h2>Characters</h2>
                    <hr />
                    {characters?.data.results.length === 0 ? (
                      <p>This story doesnt have characters</p>
                    ) : (
                      <ComicCharacters characters={characters} />
                    )}
                  </div>
                  <div className="items__additionals">
                    <h2>Comics</h2>
                    <hr />
                    {comics?.data.results.length === 0 ? (
                      <p>This story doesnt have comics</p>
                    ) : (
                      <CharacterComics comics={comics} />
                    )}
                  </div>
                </div>
              ))
            )}
          </>
        )}
      </div>
    </section>
  );
};

export default StoryDetail;
