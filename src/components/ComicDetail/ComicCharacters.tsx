import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { ICharacter } from '../../interfaces/characters';
import { AppState } from '../../redux/reducers';

type CharacterProps = {
  characters: ICharacter | undefined;
};

const ComicCharacters = ({ characters }: CharacterProps) => {
  const resourceHidden: number[] = useSelector((state: AppState) => state.hides);

  return (
    <div className="char-comics__container">
      {characters?.data.results.map(character => (
        <div
          key={character.id}
          className={`item__div ${resourceHidden.includes(character.id) ? 'display__none' : ''}`}
        >
          <div className="image__container">
            <Link to={`/characters/${character.id}`} replace>
              <img
                src={`${character.thumbnail.path}.${character.thumbnail.extension}`}
                alt={`${character.name}`}
              />
            </Link>
          </div>
          <div className="text__container">
            <h5>
              <Link to={`/comics/${character.id}`}>{character.name}</Link>
            </h5>
          </div>
        </div>
      ))}
    </div>
  );
};

export default ComicCharacters;
