import React from 'react';
import { Link } from 'react-router-dom';
import RoutesNames from '../customroutes/routesNames';

const Footer = () => {
  return (
    <footer>
      <div className="logo">
        <Link to={RoutesNames.home}>
          <h4>Marvel</h4>
        </Link>
      </div>
      <div className="footer__links">
        <div>
          <h4>Navigation</h4>
          <ul>
            <li>
              <Link to={RoutesNames.home}>Home</Link>
            </li>
            <li>
              <Link to={RoutesNames.characters}>Characters</Link>
            </li>
            <li>
              <Link to={RoutesNames.comics}>Comics</Link>
            </li>
            <li>
              <Link to={RoutesNames.stories}>Stories</Link>
            </li>
          </ul>
        </div>
        <div>
          <h4>Account</h4>
          <ul>
            <li>
              <Link to={RoutesNames.bookmarks}>Bookmarks</Link>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  );
};
export default Footer;
