import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { IStories } from '../../interfaces/stories';
import { AppState } from '../../redux/reducers';

type CharacterProps = {
  stories: IStories | undefined;
};
const CharacterStories = ({ stories }: CharacterProps) => {
  const resourceHidden: number[] = useSelector((state: AppState) => state.hides);

  return (
    <div className="char-comics__container">
      {stories?.data.results.map(story => (
        <div
          key={story.id}
          className={`item__div ${resourceHidden.includes(story.id) ? 'display__none' : ''}`}
        >
          <div className="image__container">
            <Link to={`/stories/${story.id}`} replace>
              <img
                src="https://m.media-amazon.com/images/I/51B+S8JiAaL.jpg"
                alt={`${story.title}`}
              />
            </Link>
          </div>
          <div className="text__container">
            <h5>
              <Link to={`/stories/${story.id}`}>
                {story.title.slice(0, 30)}
                ...
              </Link>
            </h5>
          </div>
        </div>
      ))}
    </div>
  );
};
export default CharacterStories;
