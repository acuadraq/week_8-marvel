import React, { useEffect, useState } from 'react';
import { getCharacters } from '../../adapters/Fetch';
import Filter from './Filter';
import Character from './Character';
import { ICharacter } from '../../interfaces/characters';

const Characters = () => {
  const [loading, setLoading] = useState(false);
  const [characters, setCharacters] = useState<ICharacter>();
  const [error, setError] = useState<string>('');

  const getInfo = (queries?: string) => {
    getCharacters(queries)
      .then(resp => {
        setCharacters(resp);
        setLoading(false);
      })
      .catch(err => {
        setError('There was an error try again');
        setLoading(false);
      });
  };

  useEffect(() => {
    setLoading(true);
    getInfo();
  }, []);

  return (
    <section>
      <div className="container">
        <div className="grid__container">
          <Filter getInfo={getInfo} />
          {loading && <div className="animation__loading" />}
          {!loading && <>{error ? <p>{error}</p> : <Character characterArray={characters} />}</>}
        </div>
      </div>
    </section>
  );
};
export default Characters;
