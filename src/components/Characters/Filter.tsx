/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { debounce } from 'lodash';
import { getComics, getStories } from '../../adapters/Fetch';
import { IComic } from '../../interfaces/comics';
import { IStories } from '../../interfaces/stories';

type filterFunc = {
  getInfo: (queries?: string) => void;
};

const Filter = ({ getInfo }: filterFunc) => {
  const [comics, setComics] = useState<IComic>();
  const [stories, setStories] = useState<IStories>();
  const [query, setQuery] = useState<string>('');
  const urlSearch = new URL(window.location.href);
  const navigate = useNavigate();
  const params = urlSearch.search;

  const getComicsFilter = async () => {
    const comicsList: IComic = await getComics();
    await setComics(comicsList);
  };

  const getStoriesFilter = async () => {
    const storiesList: IStories = await getStories();
    await setStories(storiesList);
  };

  const setName = (value: string) => {
    urlSearch.searchParams.set('nameStartsWith', value);
    navigate(`${urlSearch.search}`);
  };

  const selectComic = (value: string) => {
    urlSearch.searchParams.set('comics', value);
    navigate(`${urlSearch.search}`);
  };

  const selectStorie = (value: string) => {
    urlSearch.searchParams.set('stories', value);
    navigate(`${urlSearch.search}`);
  };

  const getName = debounce(setName, 900);

  useEffect(() => {
    getComicsFilter();
    getStoriesFilter();
  }, []);

  if (params) {
    const nameParam = urlSearch.searchParams.get('nameStartsWith');
    const comicsParam = urlSearch.searchParams.get('comics');
    const storiesParam = urlSearch.searchParams.get('stories');
    const queryNew = `${nameParam ? `&nameStartsWith=${nameParam}` : ''}${
      comicsParam ? `&comics=${comicsParam}` : ''
    }${storiesParam ? `&stories=${storiesParam}` : ''}`;
    if (query !== queryNew) {
      getInfo(queryNew);
      setQuery(queryNew);
    }
  }

  return (
    <div className="filter__container">
      <p>Filter By:</p>
      <hr />
      <div>
        <label htmlFor="search">
          Name
          <input
            type="text"
            name="search"
            id="search"
            placeholder="Characters Name"
            onChange={e => getName(e.target.value)}
          />
        </label>
      </div>
      <div>
        <label htmlFor="comic">
          Comic
          <select name="comic" id="comic" onChange={e => selectComic(e.target.value)}>
            <option value="">All</option>
            {comics?.data.results.map(comic => (
              <option key={comic.id} value={comic.id}>
                {comic.title}
              </option>
            ))}
          </select>
        </label>
      </div>
      <div>
        <label htmlFor="stories">
          Stories
          <select name="stories" id="stories" onChange={e => selectStorie(e.target.value)}>
            <option value="">All</option>
            {stories?.data.results.map(story => (
              <option key={story.id} value={story.id}>
                {story.title}
              </option>
            ))}
          </select>
        </label>
      </div>
    </div>
  );
};
export default Filter;
