/* eslint-disable implicit-arrow-linebreak */
import { ICharacter } from '../interfaces/characters';
import { IComic } from '../interfaces/comics';
import { IStories } from '../interfaces/stories';

const fetchApi = async <T>(extension: string, limit?: number, queries?: string): Promise<T> => {
  const request: Response = await fetch(
    `https://gateway.marvel.com/v1/public${extension}?ts=1&apikey=2a71e98d4c9696c23c76c0d5cff81bb8&hash=74bd1aa9a61e44bb6057bc0f91d6e904&limit=${limit}${
      queries || ''
    }`,
  );
  const response = await request.json();
  return response;
};

export const getCharacters = (queries?: string) => fetchApi<ICharacter>('/characters', 40, queries);

export const getCharacter = (id?: number) => fetchApi<ICharacter>(`/characters/${id}`, 1);

export const getCharacterComics = (id?: number) => fetchApi<IComic>(`/characters/${id}/comics`, 15);

export const getCharacterStories = (id?: number) =>
  fetchApi<IStories>(`/characters/${id}/stories`, 15);

export const getComics = (queries?: string) => fetchApi<IComic>('/comics', 35, queries);

export const getComic = (id: number) => fetchApi<IComic>(`/comics/${id}`, 1);

export const getComicCharacters = (id: number) =>
  fetchApi<ICharacter>(`/comics/${id}/characters`, 15);

export const getComicStories = (id: number) => fetchApi<IStories>(`/comics/${id}/stories`, 15);

export const getStories = (queries?: string) => fetchApi<IStories>('/stories', 35, queries);

export const getStory = (id: number) => fetchApi<IStories>(`/stories/${id}`, 1);

export const getStoryCharacters = (id: number) =>
  fetchApi<ICharacter>(`/stories/${id}/characters`, 15);

export const getStoryComics = (id: number) => fetchApi<IComic>(`/stories/${id}/comics`, 15);
