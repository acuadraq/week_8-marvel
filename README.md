# Week_8 Marvel API

![](public/Pic.png)

## Description

This is the weekly assignment for the eigth week. Its a web consuming the Marvel API. This are the main points of the page:

- This website is using React, React Router, Redux and Typescript
- This website consumes the Marvel API
- Contains two different layouts
- There is a page for every list (characters, comics, stories)
- You can filter every list and the pagination will work with that
- You can bookmark whatever you want (characters, comics, stories)
- You can hide a resource forever
- The data on the bookmarks or the hide list will persist (redux-persist)

If you want to tested in real time use this url: https://week-8-marvel.vercel.app/
